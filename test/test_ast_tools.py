""" Unit tests covering numpydoctor.ast_tools """

import ast
from pathlib import Path
from numpydoctor import ast_tools

_FIXTURE_MODULE_PATH = Path(__file__).parent / '_fixtures' / 'pymodule.py'


def test_transform_file_offset():
    source = '\n'.join([
        '0123456789',
        '012345',
        '',
        '0'
    ])

    def do_test(offset: int, expected_lineno: int, expected_col: int):
        lineno, col = ast_tools._transform_file_offset(source, offset)
        assert lineno == expected_lineno and col == expected_col, \
            f"actual {lineno}:{col} != expected {expected_lineno}:{expected_col}"

    yield do_test, 0, 1, 0
    yield do_test, 1, 1, 1
    yield do_test, 10, 1, 10
    yield do_test, 11, 2, 0
    yield do_test, 18, 3, 0
    yield do_test, 19, 4, 0
    yield do_test, 20, 4, 1


def test_ASTWalker():
    tree = ast.parse("""
def a():
    def b():
        def c(x, y, z, *args, **kwargs):
            pass
        return c
    def d():
        pass
    return b(), d

def e():
    return a()

""")

    def pre_factory(base):
        class PreTestWalker(base):
            def pre_visit(self, node):
                if hasattr(node, 'name'):
                    return node.name
        return PreTestWalker

    def post_factory(base):
        class PostTestWalker(base):
            def post_visit(self, node):
                if hasattr(node, 'name'):
                    return node.name
        return PostTestWalker

    def test_walker():
        pre_result = pre_factory(ast_tools.ASTWalker)().walk(tree)
        assert pre_result == 'a'
        post_result = post_factory(ast_tools.ASTWalker)().walk(tree)
        assert post_result == 'c'

    def test_iter_walker():
        pre_result = list(pre_factory(ast_tools.ASTIterWalker)().walk(tree))
        assert pre_result == ['a', 'b', 'c', 'd', 'e']
        post_result = list(post_factory(ast_tools.ASTIterWalker)().walk(tree))
        assert post_result == ['c', 'b', 'd', 'a', 'e']

    yield test_walker
    yield test_iter_walker


def test_documentable_node_at_point():
    with open(_FIXTURE_MODULE_PATH, 'r') as f:
        source = f.read()
    tree = ast.parse(source)

    def do_test(lineno: int, col: int, expected_node_name: str, kwargs: dict = None):
        node = ast_tools.documentable_node_at_point(tree, source, lineno=lineno, col=col, **(kwargs or {}))
        if expected_node_name is not None:
            assert hasattr(node, 'name'), f"AST node {node} has no name"
            assert node.name == expected_node_name, f"AST node name '{node.name}' != expected '{expected_node_name}'"
        else:
            assert isinstance(node, ast.Module), f"Expected instance of ast.Module, got {node}"

    yield do_test, 1, 0, None
    yield do_test, 18, 17, None
    yield do_test, 20, 0, None
    yield do_test, 21, 0, 'MyClass'
    yield do_test, 29, 71, 'MyClass'
    yield do_test, 35, 3, 'MyClass'
    yield do_test, 35, 4, '__init__'
    yield do_test, 35, 4, 'MyClass', {'magic': False}
    yield do_test, 38, 0, 'MyClass'
    yield do_test, 40, 75, 'my_method'
    yield do_test, 66, 86, '_where_am_i'
    yield do_test, 70, 23, 'nested'
    yield do_test, 70, 23, '_method_without_docstring', {'nested': False}
    yield do_test, 70, 23, 'MyClass', {'private': False}
    yield do_test, 71, 0, None
    yield do_test, 99, 6, '_my_function'
    yield do_test, 120, 0, None
    yield do_test, 121, 0, 'my_coroutine'
    yield do_test, 125, 33, 'my_coroutine'
    yield do_test, 131, 0, None


def test_query_ast():
    with open(_FIXTURE_MODULE_PATH, 'r') as f:
        source = f.read()
    tree = ast.parse(source)

    def names(results):
        for node in results:
            if hasattr(node, 'id'):
                yield node.id
            elif hasattr(node, 'name'):
                yield node.name
            elif hasattr(node, 'arg'):
                yield node.arg

    def do_test(predicate, expected_results):
        results = list((n.lineno, n.col_offset) for n in ast_tools.query_ast(tree, predicate))
        assert results == expected_results, f"Actual query results {results} != expected {expected_results}"

    def is_int(node: ast.AST) -> bool:
        return hasattr(node, 'annotation') and hasattr(node.annotation, 'id') and node.annotation.id == 'int'

    def is_str(node: ast.AST) -> bool:
        return hasattr(node, 'annotation') and hasattr(node.annotation, 'id') and node.annotation.id == 'str'

    yield do_test, is_int, [(39, 24), (39, 32), (68, 40), (77, 4), (94, 11), (96, 8), (121, 23)]

    yield do_test, is_str, [(29, 4), (65, 20), (79, 4), (102, 17), (113, 25), (117, 23)]
